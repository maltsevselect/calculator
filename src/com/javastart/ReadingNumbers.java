package com.javastart;

import java.util.Scanner;

public class ReadingNumbers {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int firstNumber, secondNumber;
        String requestNumber1 = "Введите число";
        String requestNumber2 = "Введите число";

        System.out.println(requestNumber1);
        firstNumber = input.nextInt();

        System.out.println(requestNumber2);
        secondNumber = input.nextInt();

        System.out.println("Максимальное число = " + maxNumber(firstNumber, secondNumber));
        if (firstNumber < 10 && secondNumber < 10) {
            System.out.println("Первое число = " + firstNumber + " Второе число = " + secondNumber);
        }
        if (firstNumber > 3 && secondNumber < 5 || firstNumber < 5 && secondNumber > 3) {
            System.out.println("Hello World!");
        }
        if (evenNumber(firstNumber) || evenNumber(secondNumber)) {
            System.out.println("Одно из чисел четное");
        }
        if (evenNumber(firstNumber)) {
            System.out.println("Четное число " + firstNumber);
        }
        if (evenNumber(secondNumber)) {
            System.out.println("Четное число " + secondNumber);
        }
    }

    private static int maxNumber(int a, int b) {
        if (a > b) {
            return a;
        } else {
            return b;
        }
    }

    private static boolean evenNumber (int a) {
        if (a % 2 == 0) {
            return true;
        } else {
            return false;
        }
    }

}
