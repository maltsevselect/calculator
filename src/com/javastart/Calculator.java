package com.javastart;

import java.util.Scanner;

public class Calculator {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int firstNumber, secondNumber, memory = -Integer.MAX_VALUE;
        String sign;

        String requestNumber = "Введите число";
        String requestSign = "Введите знак (+, -, /, *, = для вывода результата или @, чтобы запомнить текущий результат)";

//Чтение первого числа и первого знака
        System.out.println(requestNumber);
        firstNumber = input.nextInt();
        System.out.println(requestSign);
        sign = input.next();
//Чтение второго и последующих чисел, а также последующих знаков
        while (!sign.equals("="))
        {
            System.out.println(requestNumber);
            secondNumber = input.nextInt();
            firstNumber = calculate(firstNumber, secondNumber, sign);
            System.out.println(requestSign);
            sign = input.next();
            while (sign.equals("@")) {
                memory = firstNumber;
                System.out.println(requestSign);
                sign = input.next();
            }
        }
        System.out.println("Полученный результат = " + firstNumber);
        if (memory != -Integer.MAX_VALUE) {
            System.out.println("Сохраненная переменная = " + memory);
        }
    }

    private static int calculate (int a, int b, String sign) {
        if (sign.equals("+")){
            return a + b;
        } else
            if (sign.equals("-")) {
                return a - b;
            } else
                if (sign.equals("/")) {
                    return a / b; //Метод возвращает только целую часть от деления, не приводим к double для упрощения
                } else
                    if (sign.equals("*")) {
                        return  a * b;
                    }
                    return -1; //Если неверный знак
    }
}
